from alpine:3.10.3

env chroot_dir /layer
env mirror     http://dl-cdn.alpinelinux.org/alpine
run apk -X ${mirror}/v3.10/main -U --allow-untrusted --root ${chroot_dir} --initdb add alpine-base

run \
  mknod -m 666 ${chroot_dir}/dev/full c 1 7    && \
  mknod -m 666 ${chroot_dir}/dev/ptmx c 5 2    && \
  mknod -m 644 ${chroot_dir}/dev/random c 1 8  && \
  mknod -m 644 ${chroot_dir}/dev/urandom c 1 9 && \
  mknod -m 666 ${chroot_dir}/dev/zero c 1 5    && \
  mknod -m 666 ${chroot_dir}/dev/tty c 5 0     && \
  true

run echo ${mirror}/v3.10/main      >  ${chroot_dir}/etc/apk/repositories \
 && echo ${mirror}/v3.10/community >> ${chroot_dir}/etc/apk/repositories \
 && apk update

run apk --root ${chroot_dir} add --update \
  acct \
  acpi \
  bind-tools \
#  bird \
  btrfs-progs \
  bwm-ng \
  ca-certificates \
  ceph-base \
  chrony \
  conntrack-tools \
  cryptsetup \
  dhcpcd \
  e2fsprogs \
  ethtool \
  eudev \
  git \
  hdparm \
  iproute2 \
  ipset \
  iptables \
  iputils \
  ipvsadm \
  jq \
  keepalived \
#  killproc \
#  libcgroup \
#  libudev \
  lshw \
  lsof \
  lvm2 \
  nano \
#  netdate \
  nfs-utils \
  nmap \
  openssh \
  open-vm-tools \
  pciutils \
  rsync \
  strace \
  sudo \
  supervisor \
  sysstat \
  tcpdump \
  tree \
  usbutils \
  util-linux \
  vim \
  whois \
  xfsprogs \
  xz \
;

run apk --no-cache add ca-certificates wget \
 && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
 && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-2.30-r0.apk \
 && apk add glibc-2.30-r0.apk \
 && rm glibc-2.30-r0.apk

run apk add squashfs-tools
entrypoint ["ash","-l","-c","mksquashfs /layer /tmp/layer.fs -comp xz -noappend 1>&2 && cat /tmp/layer.fs |base64"]
